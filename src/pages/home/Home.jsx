import React from 'react';
import './Home.css';
///import './reset.css'
import porco_classico from '../../assets/images/porco_classico.png'
import senhora_cadeira from '../../assets/images/senhora_cadeira.svg'
import senhora_encontro from '../../assets/images/senhora_encontro.svg'
import subindo from '../../assets/images/subindo.svg'
import porco_escada_moeda from '../../assets/images/porco_escada_moeda.svg'
import { Link } from 'react-router-dom';


function Homepage() {
  return (
    <div className="Home">
        <div className="container-apresentacao">
          <div className="texto-pig-bank">
            <div></div>
            <div>
              <h1>Pig-Bank</h1>
              <p class="txt-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a leo 
                eu enim auctor faucibus id eget dolor. 
                Pellentesque faucibus leo sed arcu vulputate rhoncus. </p>
                <Link to="/construcao">
                  <button>NOVA META</button>
                </Link>
            </div>
            <div></div> 
          </div>
          <img src={porco_classico} id="porcoclassico"/>
        </div>

        <div class="container-comousar">
          <h1 class="titulo-comousar">Como usar</h1>
          <div class="como-usar">
            <div></div>
            <img src={senhora_cadeira} id="img-comousar"/>
            <img src={subindo} id="img-comousar"/>     
            <img src={senhora_encontro} id="img-comousar"/>
            <div></div>
          </div>
          <div class="como-usartxt">
            <div></div>
            <div class="txt">
              <h2 class="txt-blocos">DEFINA UMA META</h2>
              <p>Pellentesque faucibus leo sed arcu vulputate rhoncus. 
                Integer sit amet blandit felis.</p>
            </div>
            <div class="txt">
              <h2 class="txt-blocos">ACOMPANHE O PROGRESSO</h2>
              <p>Pellentesque faucibus leo sed arcu vulputate rhoncus. 
                Integer sit amet blandit felis.</p>
            </div>
            <div class="txt">
              <h2 class="txt-blocos">ALCANCE SEU OBJETIVO</h2>
              <p>Pellentesque faucibus leo sed arcu vulputate rhoncus. 
                Integer sit amet blandit felis.</p>
            </div>
            <div></div>              
          </div>

        </div>

        
        <div class="container-jatemmeta">
          <div className="texto-jatemmeta">
            <div></div>
            <div>
              <h1>Já tem uma meta?</h1>
              <p class="txt-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a leo 
                eu enim auctor faucibus id eget dolor. 
                Pellentesque faucibus leo sed arcu vulputate rhoncus. </p>
                <Link to="/construcao">
                  <button>VER METAS</button>
                </Link>
            </div>

            <div></div>
          </div>
          <div>
            <img src={porco_escada_moeda} id="img-comousar"/>
          </div>
        </div>
        <div class='txt-final'>
          <h1>Não tem? Comece agora mesmo</h1>
          <Link to="/construcao">
            <button>NOVA META</button>
          </Link>
        </div>
        
        
    </div>
  );
}

export default Homepage;