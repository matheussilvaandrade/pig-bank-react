import React from 'react';
import './404.css';
import porco_confuso from '../../assets/images/porco_confuso.svg'
import { Link } from 'react-router-dom';



function NaoEncontrada() {
  return (
    <div className="Missing">
        <main class="container-404">
          <h1>ERRO 404</h1>
          <img src={porco_confuso} id="porco404"/>
          <p class="txt-margin">Ops! Parece que o endereço que você digitou está incorreto...</p>
          <Link to="/">
            <button>VOLTAR</button>
          </Link>          

        </main>
    </div>
  );
}

export default NaoEncontrada;