import React from 'react';
import './Construcao.css';
import soon from '../../assets/images/soon.svg'
import { Link } from 'react-router-dom';



function Construcao() {
  return (
    <div className="Construcao">
        <main class="container-soon">
          <h1>Em construção</h1>
          <img src={soon} id="porcoconstrucao"/>
          <p>Parece que essa página ainda não foi implementada... </p>
          <p class="txt-margin">Tente novamente mais tarde!</p>
          <Link to="/">
            <button>VOLTAR</button>
          </Link>

        </main>
    </div>
  );
}

export default Construcao;