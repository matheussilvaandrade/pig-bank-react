import React, { useState, useEffect } from 'react';
import { cotacao_hj } from '../../services/Api-cotacao';
import './Cotacao.css'



function CotacaoHj() {
    const [resposta, setResposta] = useState([])
    useEffect(()=>{
      cotacao_hj(setResposta)
    }, [])
    ///console.log(resposta)

    return(
        resposta.map(moeda => {
            return(
                <div>
                    <h1>{moeda.code}</h1>
                    <h1>{moeda.name}</h1>
                    <h1>{moeda.high}</h1>
                    <h1>{moeda.create_data}</h1>
                </div>
            )
        })
                 
    )
}

export default CotacaoHj;