import React from 'react'




function arrumaData(data){
    let dateTime = '';
    let timeStamp = data.create_date.split(" ");
    let currentDayList = timeStamp[0].split("-");
    let currentDay = currentDayList[2] + "/" + currentDayList[1] + "/" + currentDayList[0];
    let currentHour = timeStamp[1];

    dateTime = "Atualizado em " + currentDay + " às " + currentHour;

    return dateTime;
}


function Coincard(props) {
    return (
        <div class="card">
            <div class="titulo">
                <h3>{props.coin.name}</h3>
                <h4>{props.coin.code}</h4>
            </div>
            <div class="preco">
                <p class="maxmin">Máxima: R$ {props.coin.high}</p>
                <p class="maxmin">Mínima: R$ {props.coin.low}</p>
                <p id='data'>{arrumaData(props.coin)}</p>

            </div>
        </div>
    )
}


export default Coincard