import React, { useState, useEffect } from 'react';
import { cotacao_hj } from '../../services/Api-cotacao';
import './Cotacao.css'
import Coincard from './Cards'


function MostraCotacao() {
    const [resposta, setResposta] = useState([])
    useEffect(()=>{
      cotacao_hj(setResposta)
    }, [])


    return(
        resposta.map(moeda => {
            return(
                <div>
                    <div class="conjunto-cards">
                        <Coincard coin={moeda}></Coincard>
                    </div>
                                       
                </div>

                
            )
        })
                 
    )
}


export default MostraCotacao