import axios from 'axios'

const api = axios.create({
    baseURL: "https://economia.awesomeapi.com.br"
})


function cotacao_hj(setResposta) {
    api.get("/json/all")
        .then(response => {
            let lista = [];
            Object.keys(response.data).forEach(e => {
                lista.push(response.data[e]);
            })
            setResposta(lista) 
        }).catch(error => {
            console.log(error)
        })
}

export { cotacao_hj }

