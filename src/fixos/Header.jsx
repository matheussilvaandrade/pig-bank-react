import React, { useState } from 'react';
import './Header.css';
import NavBar from './Nav-bar';
///import  {  slide  as  Menu  }  from  'react-burger-menu'


function Header() {
  function toggle(e) {
    console.log(e.target)
    if(status === 'open') {
      setStatus('close')
    } else {
      setStatus('open')
    }
  }
  const [status, setStatus] = useState('close');
  return (
    <div class="container-menu">
      <input type="checkbox" id="nav-menu1"></input>
      <label id="nav-icon1" for="nav-menu1" className={status} onClick={(e) => (toggle(e))}>
        <span></span>
        <span></span>
        <span></span>
        <NavBar click={(e) => (toggle(e))}></NavBar>
      </label>
      
    </div>
  );
}

export default Header;

