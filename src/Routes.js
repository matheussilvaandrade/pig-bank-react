import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Footer from './fixos/Footer';
import Header from './fixos/Header';
import NaoEncontrada from './pages/404/404';
import MostraCotacao from './pages/cotacao/Cotacao';
import Homepage from './pages/home/Home';
import Construcao from './pages/construcao/Construcao.jsx'


function Routes() {
    return(
        <BrowserRouter>
            <Header></Header>
            <Switch>
                <Route exact path="/cotacao">
                    <MostraCotacao></MostraCotacao>
                    <div class="espaco"></div>
                </Route>

                <Route exact path="/">
                    <Homepage></Homepage>
                </Route>

                

                <Route exact path="/construcao">
                    <Construcao></Construcao>
                </Route>

                <Route path="/">
                    <NaoEncontrada></NaoEncontrada>
                </Route>

            </Switch>
            <Footer></Footer>
        </BrowserRouter>
    )

}

export default Routes