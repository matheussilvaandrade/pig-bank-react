import React from 'react';
import Routes from './Routes'
import './App.css';
import Construcao from './pages/construcao/Construcao';


function App() {
  return (
    <div className="App">
      
      <Routes></Routes>
      
    </div>
  );
}

export default App;
